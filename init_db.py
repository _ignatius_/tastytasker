from fastapi import Depends
from db import Base, engine
from entity import User
from sqlalchemy import select
import typing
from sqlalchemy.ext.asyncio import AsyncSession
from db import async_session_maker

from repository import user_rep, task_rep, task_stats_rep

from entity import User, Task


async def get_async_session() -> typing.AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session

async def create_db_and_tables():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

async def populate_tables():
    async with async_session_maker() as session:
        await user_rep.add_users(session,[{"name":"user1"}, {"name":"user2"}])
        await task_stats_rep.add_task_stats(session)