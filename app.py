from fastapi import FastAPI, Depends
from repository import task_rep, user_rep, task_stats_rep
from sqlalchemy.orm import Session, declarative_base
from sqlalchemy.ext.asyncio import AsyncSession, async_session
import typing
from init_db import get_async_session, create_db_and_tables, populate_tables
import datetime
from pydantic import BaseModel

app = FastAPI()

class TaskModel(BaseModel):
    id: typing.Optional[int] = None
    name: str
    created_at: datetime.datetime = None
    user_id: typing.Optional[int] 
    
class UserModel(BaseModel):
    id: typing.Optional[int] = None
    name: str
    created_at: datetime.datetime = None


class StatisticsModel(BaseModel):
    ongoing_tasks: typing.List[TaskModel]
    number_tasks: int
    number_finished_tasks: int

@app.on_event("startup")
async def on_startup():
    await create_db_and_tables()
    await populate_tables()

@app.get("/get_task_statistics")
async def get_task_statistics(session:AsyncSession = Depends(get_async_session)) -> StatisticsModel:
    ongoing_tasks = await task_rep.get_active_tasks(session)
    task_stats = await task_stats_rep.get_task_stats(session)
    task_stats = task_stats.scalar()
    return StatisticsModel(ongoing_tasks=[TaskModel(id=ot.id, created_at= ot.created_at, user_id=ot.user_id, name= ot.name) for ot in ongoing_tasks.scalars().all()], number_tasks=task_stats.number_tasks, number_finished_tasks=task_stats.number_finished_tasks)

@app.put("/add_tasks_to_user/{user_id}", response_model=None)
async def add_tasks(user_id, tasks: typing.List[UserModel], session: AsyncSession = Depends(get_async_session)):
    number_tasks = await user_rep.get_number_of_tasks(user_id, session)
    number_tasks = number_tasks.scalar()
    new_tasks = len(tasks)
    if((number_tasks+new_tasks)>=10):
        user_tasks = await(task_rep.get_by_user_id(user_id, session))
        user_tasks = user_tasks.scalars().all()
        to_be_finished = [user_tasks[i] for i in range(abs(10-(number_tasks+new_tasks)))]
        await task_rep.finish_tasks(session, to_be_finished)
        await task_stats_rep.update_task_stats(session, number_finished_incr=abs(10-(number_tasks+new_tasks)))

    await user_rep.add_tasks_to_user(user_id, tasks, session)
    await task_stats_rep.update_task_stats(session, number_tasks_incr=len(tasks))

@app.put("/add_five_tasks/{user_id}")
async def add_five_tasks(user_id: int, tasks: typing.List[UserModel], session: AsyncSession = Depends(get_async_session)):
    if(len(tasks)!=5):
        return {"response": "This endpoint needs exactly five tasks."}
    await add_tasks(user_id, tasks, session)

@app.put("/finish_task/{task_id}", response_model=None)
async def finish_task(task_id: int, session:AsyncSession = Depends(get_async_session)):
    await task_rep.finish_task(session, task_id)
    await task_stats_rep.update_task_stats(session, number_finished_incr=1)


@app.get("/get_tasks")
async def get_tasks(session:AsyncSession = Depends(get_async_session))-> typing.List[TaskModel]:
    tasks = await task_rep.get_all_tasks(session)
    return [TaskModel(id=t.id, name=t.name, created_at=t.created_at, user_id=t.user_id) for t in tasks.scalars().all()]

@app.get("/get_tasks_for_user/{user_id}")
async def get_tasks_for_user(user_id, session:AsyncSession = Depends(get_async_session))-> typing.List[TaskModel]:
    tasks = await task_rep.get_by_user_id(user_id, session)
    return [TaskModel(name=t.name, user_id=t.user_id) for t in tasks.scalars().all()]

@app.get("/get_users")
async def get_users(session: AsyncSession = Depends(get_async_session))-> typing.List[UserModel]:
    users = await user_rep.get_users(session)
    return [UserModel(id=u.id, name=u.name, created_at=u.created_at) for u in users.scalars().all()]

@app.get("/get/user/{id}")
async def get_user(id: int, session: AsyncSession = Depends(get_async_session)) -> UserModel:
    user = (await user_rep.get_user_by_id(id, session)).scalar()
    return UserModel(id=user.id, name=user.name, created_at=user.created_at)