import typing
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

Base = declarative_base()

engine = create_async_engine("sqlite+aiosqlite:///:memory:", echo=True)
async_session_maker = async_sessionmaker(engine)




