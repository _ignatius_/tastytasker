from db import Base
from typing import List
from sqlalchemy import Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import mapped_column, relationship, Mapped
from sqlalchemy.sql import func
import datetime

class User(Base):
    __tablename__="user"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(32))
    tasks: Mapped[List["Task"]] =  relationship(back_populates="user")
    created_at: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True), server_default=func.now())

