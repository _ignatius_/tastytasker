from db import Base
from sqlalchemy import Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import mapped_column, relationship, Mapped
import datetime
from sqlalchemy.sql import func

class TaskStats(Base):
    __tablename__="task_info"
    id: Mapped[int] = mapped_column(primary_key=True)
    number_tasks: Mapped[int] = mapped_column(Integer)
    number_finished_tasks: Mapped[int] = mapped_column(Integer)

