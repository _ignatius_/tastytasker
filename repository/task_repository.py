from entity import Task, User
from sqlalchemy import select, and_
from sqlalchemy.ext.asyncio import AsyncSession
from db import engine
from sqlalchemy.orm import selectinload
from sqlalchemy.sql import func

class TaskRepository:

    async def get_all_tasks(self, session: AsyncSession):
        tasks = await session.execute(select(Task))
        return tasks

    async def get_finished_tasks(self, session: AsyncSession):
        tasks = await session.execute(select(Task).where(Task.deleted_at!=None))
        return tasks

    async def get_active_tasks(self, session: AsyncSession):
        tasks = await session.execute(select(Task).where(Task.deleted_at == None))
        return tasks


    async def add_tasks(self, session: AsyncSession, tasks):
        tasks = [Task(name=t.name) for t in tasks.tasks]
        session.add_all(tasks)
        await session.commit()

    async def get_by_user_id(self, user_id: int, session: AsyncSession):
        tasks = await session.execute(select(Task).where(and_(Task.user_id==user_id, Task.deleted_at == None)).order_by(Task.created_at.asc()))
        return tasks

    async def finish_task(self, session: AsyncSession, task_id: int):
        task = await session.get(Task, task_id)
        print(task)
        task.deleted_at = func.now()
        await session.commit()

    async def finish_tasks(self, session: AsyncSession, tasks):
        for t in tasks:
            t.deleted_at = func.now()
        await session.commit()
        

task_rep = TaskRepository()