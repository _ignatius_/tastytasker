from entity import User, Task
from db import engine
from sqlalchemy import select, and_
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from sqlalchemy.sql import func

class UserRepository:

    async def add_users(self, session: AsyncSession, user_data):
        new_users = [User(**user) for user in user_data]
        session.add_all(new_users)
        await session.commit()

    async def get_users(self, session: AsyncSession):
         users = await session.execute(select(User))
         return users

    async def get_user_by_id(self, user_id, session: AsyncSession):
        user = await session.execute(select(User).where(User.id==user_id))
        return user

    async def get_number_of_tasks(self, user_id, session: AsyncSession):
        task_number = await session.execute(select(func.count()).select_from(Task).where(and_(Task.user_id == user_id, Task.deleted_at == None)))
        return task_number

    async def add_tasks_to_user(self, user_id, tasks, session):
        user = await session.execute(select(User).where(User.id==user_id).options(selectinload(User.tasks)))
        user = user.scalar()
        user.tasks.extend([Task(name=t.name, user=user, user_id=user.id) for t in tasks])
        await session.commit()

user_rep = UserRepository()