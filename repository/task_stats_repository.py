from entity import TaskStats
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from db import engine

class TaskStatsRepository:

    async def get_task_stats(self, session: AsyncSession):
        task_stats = await session.execute(select(TaskStats))
        return task_stats

    async def update_task_stats(self, session: AsyncSession, number_tasks_incr = 0, number_finished_incr=0):
        task_stats = await session.execute(select(TaskStats))
        task_stats = task_stats.scalar()
        task_stats.number_tasks += number_tasks_incr
        task_stats.number_finished_tasks += number_finished_incr
        await session.commit()

    async def add_task_stats(self, session:AsyncSession):
        session.add(TaskStats(number_tasks=0, number_finished_tasks=0))
        await session.commit()

task_stats_rep = TaskStatsRepository()